//Lee Vecchiarelli, 2035024
public class BikeStore {
    public static void main(String[]args){
        Bicycle bikes[] = new Bicycle[4];

        bikes[0] = new Bicycle("Nissan", 12, 6);
        bikes[1] = new Bicycle("Toys r Us", 30, 20);
        bikes[2] = new Bicycle("Mercedes", 2, 60);
        bikes[3] = new Bicycle("Toyoda", 120, 5);

        for(int i=0; i<bikes.length; i++){
            System.out.println(bikes[i]);
        }
    }
}
